var webpack = require('webpack'),
    path = require('path'),
    ExtractTextPlugin = require("extract-text-webpack-plugin"),
    isProduction = process.env.NODE_ENV === 'production';

module.exports = {

  entry: {

    app: [
      './assets/src/js/app.js',
      './assets/src/less/ab.less'
    ],

    envato: './assets/src/js/envato.js'

  },

  output: {

    path: path.resolve(__dirname, './assets/build/'),
    publicPath: '/assets/build/',
    filename: 'js/[name].js'

  },

  resolve: {
    alias: {
      'vue$': 'vue/dist/vue.esm.js',
      'components': path.resolve(__dirname, 'assets/src/js/components/'),
      'modules': path.resolve(__dirname, 'assets/src/js/modules/')
    }
  },

  module: {

    rules: [

      {
        test: /\.js$/,
        exclude: /node_modules/,
        loader: "babel-loader"
      },

      {
        test: /\.vue$/,
        loader: 'vue-loader'
      },

      {
        test: /\.less$/,
        use: ExtractTextPlugin.extract({
          use: [
            {
              loader: 'css-loader',
              options: {
                minimize: isProduction
              }
            },
            'less-loader'
          ],
          fallback: 'style-loader'
        })
      },

      {
        test: /\.css$/,
        use: 'css-loader'
      },

      {
        test: /\.(png|jpg|jpeg|gif)$/,
        loader: 'file-loader',
        options: {
          name: 'img/[name].[ext]'
        }
      },

      {
        test: /\.(otf|eot|svg|ttf|woff|woff2)$/,
        loader: 'file-loader',
        options: {
          name: 'fonts/[name].[ext]',
          publicPath: '../'
        }
      }

    ]
  },

  plugins: [

    new ExtractTextPlugin('css/[name].css'),

    new webpack.optimize.CommonsChunkPlugin({
      name: "vendor",
      minChunks: function (module) {
        return module.context && module.context.indexOf("node_modules") !== -1;
      }
    })

  ],

  devServer: {
    contentBase: path.join(__dirname, ""),
    compress: true,
    port: 9000,
    quiet: true
  }
};

if (isProduction) {
  module.exports.plugins = module.exports.plugins.concat([

    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: '"production"'
      }
    }),

    new webpack.optimize.UglifyJsPlugin({
      sourceMap: true,
      compress: {
        warnings: false
      }
    })

  ])
}
