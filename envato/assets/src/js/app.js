require('es6-promise').polyfill();
require('classlist-polyfill');
require('js-polyfills/url.js');

import Vue from 'vue';
import Velocity from 'velocity-animate';
import { formatNumber, addReferral } from './utils/filters.js';

Vue.filter('formatNumber', formatNumber);
Vue.filter('addReferral', addReferral);

window.App = new Vue({
  el: '#app',
  data: function() {
    return {}
  },
  methods: {
    scrollTo: function(element, duration = 500) {
      Velocity(element, 'scroll', { duration });
    }
  }
})
