/**
 * formatNumber - Numbers formatting
 *
 * @param  {number} n               Number
 * @param  {string} currency = ''   Currency ('$','€', etc.)
 * @param  {number} a               Digits after the decimal point
 * @return {string}                 Formatted string ('-$12,000.50')
 */
export function formatNumber(n, currency = '', a) {
    let c = isNaN(c = Math.abs(a)) ? 0 : a,
    d = d == undefined ? "." : d,
    t = t == undefined ? "," : t,
    s = n < 0 ? "-" : "",
    i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "",
    j = (j = i.length) > 3 ? j % 3 : 0;
   return s + currency +(j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
}

/**
 * addReferral - Adds referral param to the given url
 *
 * @param  {string} link            URL
 * @return {string}                 Reffral URL (http://example.com?ref=AdminBootstrap)
 */
export function addReferral(link) {
  let url = link;
  try {
    url = new URL(link)
    url.searchParams.set('ref', 'AdminBootstrap');
  } catch(e) {
    if (process.env.NODE_ENV !== 'production') {
      console.warn('Failed to add reffral: ' + link);
    }
    url = '';
  }
  return url.toString();
}
