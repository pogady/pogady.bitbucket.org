import Vue from 'vue';
import EnvCatalog from './components/envato/env-catalog.vue';

Vue.component('env-catalog', EnvCatalog);
