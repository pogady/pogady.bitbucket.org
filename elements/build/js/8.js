webpackJsonp([8],{

/***/ 435:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_bustCache_buttons_vue__ = __webpack_require__(436);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__node_modules_vue_loader_lib_template_compiler_index_id_data_v_03ee345e_hasScoped_false_buble_transforms_node_modules_vue_loader_lib_selector_type_template_index_0_bustCache_buttons_vue__ = __webpack_require__(437);
var disposed = false
var normalizeComponent = __webpack_require__(6)
/* script */

/* template */

/* template functional */
  var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = null
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_bustCache_buttons_vue__["a" /* default */],
  __WEBPACK_IMPORTED_MODULE_1__node_modules_vue_loader_lib_template_compiler_index_id_data_v_03ee345e_hasScoped_false_buble_transforms_node_modules_vue_loader_lib_selector_type_template_index_0_bustCache_buttons_vue__["a" /* default */],
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "src\\js\\bootstrap\\sections\\buttons.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {  return key !== "default" && key.substr(0, 2) !== "__"})) {  console.error("named exports are not supported in *.vue files.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-03ee345e", Component.options)
  } else {
    hotAPI.reload("data-v-03ee345e", Component.options)
' + '  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

/* harmony default export */ __webpack_exports__["default"] = (Component.exports);


/***/ }),

/***/ 436:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["a"] = ({});

/***/ }),

/***/ 437:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "row" }, [
    _c("div", { staticClass: "col-lg-8 mb-3" }, [
      _c("div", { staticClass: "card mb-3" }, [
        _c("div", { staticClass: "card-body" }, [
          _c("h5", { staticClass: "card-title" }, [_vm._v("Different Colors")]),
          _vm._v(" "),
          _vm._m(0),
          _vm._v(" "),
          _c("p", { staticClass: "card-text mb-3" }, [
            _c(
              "a",
              {
                staticClass: "btn btn-primary",
                attrs: { href: "#" },
                on: {
                  click: function($event) {
                    $event.preventDefault()
                  }
                }
              },
              [_vm._v("Primary")]
            ),
            _vm._v(" "),
            _c(
              "a",
              {
                staticClass: "btn btn-secondary",
                attrs: { href: "#" },
                on: {
                  click: function($event) {
                    $event.preventDefault()
                  }
                }
              },
              [_vm._v("Secondary")]
            ),
            _vm._v(" "),
            _c(
              "a",
              {
                staticClass: "btn btn-success",
                attrs: { href: "#" },
                on: {
                  click: function($event) {
                    $event.preventDefault()
                  }
                }
              },
              [_vm._v("Success")]
            ),
            _vm._v(" "),
            _c(
              "a",
              {
                staticClass: "btn btn-danger",
                attrs: { href: "#" },
                on: {
                  click: function($event) {
                    $event.preventDefault()
                  }
                }
              },
              [_vm._v("Danger")]
            ),
            _vm._v(" "),
            _c(
              "a",
              {
                staticClass: "btn btn-warning",
                attrs: { href: "#" },
                on: {
                  click: function($event) {
                    $event.preventDefault()
                  }
                }
              },
              [_vm._v("Warning")]
            ),
            _vm._v(" "),
            _c(
              "a",
              {
                staticClass: "btn btn-info",
                attrs: { href: "#" },
                on: {
                  click: function($event) {
                    $event.preventDefault()
                  }
                }
              },
              [_vm._v("Info")]
            ),
            _vm._v(" "),
            _c(
              "a",
              {
                staticClass: "btn btn-light",
                attrs: { href: "#" },
                on: {
                  click: function($event) {
                    $event.preventDefault()
                  }
                }
              },
              [_vm._v("Light")]
            ),
            _vm._v(" "),
            _c(
              "a",
              {
                staticClass: "btn btn-dark",
                attrs: { href: "#" },
                on: {
                  click: function($event) {
                    $event.preventDefault()
                  }
                }
              },
              [_vm._v("Dark")]
            ),
            _vm._v(" "),
            _c(
              "a",
              {
                staticClass: "btn btn-link",
                attrs: { href: "#" },
                on: {
                  click: function($event) {
                    $event.preventDefault()
                  }
                }
              },
              [_vm._v("Link")]
            )
          ]),
          _vm._v(" "),
          _c("h5", { staticClass: "card-title" }, [_vm._v("Outline Buttons")]),
          _vm._v(" "),
          _c("p", { staticClass: "card-text" }, [
            _vm._v(
              "In need of a button, but not the hefty background colors they bring?"
            )
          ]),
          _vm._v(" "),
          _c("p", { staticClass: "card-text" }, [
            _c(
              "a",
              {
                staticClass: "btn btn-outline-primary",
                attrs: { href: "#" },
                on: {
                  click: function($event) {
                    $event.preventDefault()
                  }
                }
              },
              [_vm._v("Primary")]
            ),
            _vm._v(" "),
            _c(
              "a",
              {
                staticClass: "btn btn-outline-secondary",
                attrs: { href: "#" },
                on: {
                  click: function($event) {
                    $event.preventDefault()
                  }
                }
              },
              [_vm._v("Secondary")]
            ),
            _vm._v(" "),
            _c(
              "a",
              {
                staticClass: "btn btn-outline-success",
                attrs: { href: "#" },
                on: {
                  click: function($event) {
                    $event.preventDefault()
                  }
                }
              },
              [_vm._v("Success")]
            ),
            _vm._v(" "),
            _c(
              "a",
              {
                staticClass: "btn btn-outline-danger",
                attrs: { href: "#" },
                on: {
                  click: function($event) {
                    $event.preventDefault()
                  }
                }
              },
              [_vm._v("Danger")]
            ),
            _vm._v(" "),
            _c(
              "a",
              {
                staticClass: "btn btn-outline-warning",
                attrs: { href: "#" },
                on: {
                  click: function($event) {
                    $event.preventDefault()
                  }
                }
              },
              [_vm._v("Warning")]
            ),
            _vm._v(" "),
            _c(
              "a",
              {
                staticClass: "btn btn-outline-info",
                attrs: { href: "#" },
                on: {
                  click: function($event) {
                    $event.preventDefault()
                  }
                }
              },
              [_vm._v("Info")]
            ),
            _vm._v(" "),
            _c(
              "a",
              {
                staticClass: "btn btn-outline-light",
                attrs: { href: "#" },
                on: {
                  click: function($event) {
                    $event.preventDefault()
                  }
                }
              },
              [_vm._v("Light")]
            ),
            _vm._v(" "),
            _c(
              "a",
              {
                staticClass: "btn btn-outline-dark",
                attrs: { href: "#" },
                on: {
                  click: function($event) {
                    $event.preventDefault()
                  }
                }
              },
              [_vm._v("Dark")]
            )
          ])
        ])
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "card mb-3" }, [
        _c("div", { staticClass: "card-body" }, [
          _c("h5", { staticClass: "card-title" }, [
            _vm._v("Button Groups and Toolbars")
          ]),
          _vm._v(" "),
          _vm._m(1),
          _vm._v(" "),
          _c("div", { staticClass: "clearfix" }, [
            _vm._m(2),
            _vm._v(" "),
            _c("div", { staticClass: "btn-toolbar pull-left" }, [
              _vm._m(3),
              _vm._v(" "),
              _c(
                "div",
                { staticClass: "btn-group" },
                [
                  _c(
                    "button",
                    { staticClass: "btn btn-info", attrs: { type: "button" } },
                    [_vm._v("Single")]
                  ),
                  _vm._v(" "),
                  _c(
                    "b-dropdown",
                    { attrs: { text: "Group", variant: "info" } },
                    [
                      _c("b-dropdown-item", [_vm._v("Dropdown Link")]),
                      _vm._v(" "),
                      _c("b-dropdown-item", [_vm._v("Dropdown Link")])
                    ],
                    1
                  )
                ],
                1
              )
            ])
          ])
        ])
      ]),
      _vm._v(" "),
      _vm._m(4)
    ]),
    _vm._v(" "),
    _c("div", { staticClass: "col-lg-4 mb-3" }, [
      _c("div", { staticClass: "card" }, [
        _c("div", { staticClass: "card-body" }, [
          _c("h5", { staticClass: "card-title" }, [_vm._v("Button Sizes")]),
          _vm._v(" "),
          _vm._m(5),
          _vm._v(" "),
          _vm._m(6),
          _vm._v(" "),
          _c("h5", { staticClass: "card-title" }, [_vm._v("Button Tags")]),
          _vm._v(" "),
          _vm._m(7),
          _vm._v(" "),
          _c("p", { staticClass: "card-text" }, [
            _c(
              "a",
              {
                staticClass: "btn btn-primary",
                attrs: { href: "#", role: "button" },
                on: {
                  click: function($event) {
                    $event.preventDefault()
                  }
                }
              },
              [_vm._v("Link")]
            ),
            _vm._v(" "),
            _c(
              "button",
              { staticClass: "btn btn-primary", attrs: { type: "button" } },
              [_vm._v("Button")]
            ),
            _vm._v(" "),
            _c("input", {
              staticClass: "btn btn-primary",
              attrs: { type: "button", value: "Input" }
            })
          ]),
          _vm._v(" "),
          _c("h5", { staticClass: "card-title" }, [_vm._v("Button States")]),
          _vm._v(" "),
          _c("p", [_vm._v("Buttons may also appear active or disabled.")]),
          _vm._v(" "),
          _c("p", { staticClass: "card-text" }, [
            _c(
              "a",
              {
                staticClass: "btn btn-primary",
                attrs: { href: "#" },
                on: {
                  click: function($event) {
                    $event.preventDefault()
                  }
                }
              },
              [_vm._v("Normal")]
            ),
            _vm._v(" "),
            _c(
              "a",
              {
                staticClass: "btn btn-primary active",
                attrs: { href: "#" },
                on: {
                  click: function($event) {
                    $event.preventDefault()
                  }
                }
              },
              [_vm._v("Active")]
            ),
            _vm._v(" "),
            _c(
              "a",
              {
                staticClass: "btn btn-primary disabled",
                attrs: { href: "#" },
                on: {
                  click: function($event) {
                    $event.preventDefault()
                  }
                }
              },
              [_vm._v("Disabled")]
            )
          ])
        ])
      ])
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("p", { staticClass: "card-text" }, [
      _vm._v("Use available button classes on an "),
      _c("code", [_vm._v("<a>")]),
      _vm._v(", "),
      _c("code", [_vm._v("<button>")]),
      _vm._v(", "),
      _c("code", [_vm._v("<input>")]),
      _vm._v(" element to quickly create a styled button.")
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("p", { staticClass: "card-text" }, [
      _vm._v("Wrap a series of buttons with "),
      _c("code", [_vm._v(".btn")]),
      _vm._v(" in "),
      _c("code", [_vm._v(".btn-group")]),
      _vm._v(". Combine sets of "),
      _c("code", [_vm._v(".btn-group")]),
      _vm._v(" into a "),
      _c("code", [_vm._v(".btn-toolbar")]),
      _vm._v(".")
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "btn-toolbar pull-left" }, [
      _c(
        "div",
        { staticClass: "btn-group mr-2", attrs: { "data-toggle": "buttons" } },
        [
          _c("label", { staticClass: "btn btn-danger active" }, [
            _c("input", {
              attrs: { type: "checkbox", checked: "", autocomplete: "off" }
            }),
            _vm._v(" Left\n              ")
          ]),
          _vm._v(" "),
          _c("label", { staticClass: "btn btn-danger active" }, [
            _c("input", { attrs: { type: "checkbox", autocomplete: "off" } }),
            _vm._v(" Center\n              ")
          ]),
          _vm._v(" "),
          _c("label", { staticClass: "btn btn-danger" }, [
            _c("input", { attrs: { type: "checkbox", autocomplete: "off" } }),
            _vm._v(" Right\n              ")
          ])
        ]
      ),
      _vm._v(" "),
      _c(
        "div",
        { staticClass: "btn-group mr-2", attrs: { "data-toggle": "buttons" } },
        [
          _c("label", { staticClass: "btn btn-primary active" }, [
            _c("input", { attrs: { type: "radio", checked: "checked" } }),
            _vm._v(" "),
            _c("i", { staticClass: "fa fa-align-left" })
          ]),
          _vm._v(" "),
          _c("label", { staticClass: "btn btn-primary" }, [
            _c("input", { attrs: { type: "radio" } }),
            _vm._v(" "),
            _c("i", { staticClass: "fa fa-align-center" })
          ]),
          _vm._v(" "),
          _c("label", { staticClass: "btn btn-primary" }, [
            _c("input", { attrs: { type: "radio" } }),
            _vm._v(" "),
            _c("i", { staticClass: "fa fa-align-justify" })
          ]),
          _vm._v(" "),
          _c("label", { staticClass: "btn btn-primary" }, [
            _c("input", { attrs: { type: "radio" } }),
            _vm._v(" "),
            _c("i", { staticClass: "fa fa-align-right" })
          ])
        ]
      )
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "btn-group mr-2" }, [
      _c("button", { staticClass: "btn btn-outline-secondary" }, [_vm._v("1")]),
      _vm._v(" "),
      _c("button", { staticClass: "btn btn-outline-secondary" }, [_vm._v("2")]),
      _vm._v(" "),
      _c("button", { staticClass: "btn btn-outline-secondary" }, [_vm._v("3")]),
      _vm._v(" "),
      _c("button", { staticClass: "btn btn-outline-secondary" }, [_vm._v("4")]),
      _vm._v(" "),
      _c("button", { staticClass: "btn btn-outline-secondary" }, [_vm._v("5")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "card" }, [
      _c("div", { staticClass: "card-body" }, [
        _c("h5", { staticClass: "card-title" }, [_vm._v("Icon Buttons")]),
        _vm._v(" "),
        _c("p", { staticClass: "card-text" }, [
          _vm._v("To any button you can add an icon. Place "),
          _c("code", [_vm._v('<i class="{icon-class}" /><span>Text</span>')]),
          _vm._v(" inside it.")
        ]),
        _vm._v(" "),
        _c("p", { staticClass: "card-text" }, [
          _c(
            "button",
            { staticClass: "btn btn-success", attrs: { type: "button" } },
            [
              _c("i", { staticClass: "fa fa-plus" }),
              _vm._v(" "),
              _c("span", [_vm._v("Create")])
            ]
          ),
          _vm._v(" "),
          _c(
            "button",
            { staticClass: "btn btn-warning", attrs: { type: "button" } },
            [
              _c("i", { staticClass: "fa fa-edit" }),
              _vm._v(" "),
              _c("span", [_vm._v("Edit")])
            ]
          ),
          _vm._v(" "),
          _c(
            "button",
            { staticClass: "btn btn-danger", attrs: { type: "button" } },
            [
              _c("i", { staticClass: "fa fa-remove" }),
              _vm._v(" "),
              _c("span", [_vm._v("Delete")])
            ]
          ),
          _vm._v(" "),
          _c(
            "button",
            { staticClass: "btn btn-info", attrs: { type: "button" } },
            [
              _c("i", { staticClass: "fa fa-refresh" }),
              _vm._v(" "),
              _c("span", [_vm._v("Reload")])
            ]
          ),
          _vm._v(" "),
          _c(
            "button",
            { staticClass: "btn btn-primary", attrs: { type: "button" } },
            [
              _c("i", { staticClass: "fa fa-send" }),
              _vm._v(" "),
              _c("span", [_vm._v("Send")])
            ]
          ),
          _vm._v(" "),
          _c(
            "button",
            { staticClass: "btn btn-secondary", attrs: { type: "button" } },
            [_c("i", { staticClass: "fa fa-heart" })]
          ),
          _vm._v(" "),
          _c(
            "button",
            {
              staticClass: "btn btn-outline-secondary",
              attrs: { type: "button" }
            },
            [
              _c("i", { staticClass: "fa fa-star" }),
              _vm._v(" "),
              _c("span", [_vm._v("Star")])
            ]
          )
        ])
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("p", { staticClass: "card-text" }, [
      _vm._v("Fancy larger or smaller buttons? Add "),
      _c("code", [_vm._v(".btn-lg")]),
      _vm._v(" or "),
      _c("code", [_vm._v(".btn-sm")]),
      _vm._v(
        " for additional sizes. Block buttons span the full width of a parent by adding "
      ),
      _c("code", [_vm._v(".btn-block")]),
      _vm._v(".")
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("p", { staticClass: "card-text" }, [
      _c(
        "button",
        {
          staticClass: "btn btn-secondary btn-lg btn-block",
          attrs: { type: "button" }
        },
        [_vm._v("Block Button")]
      ),
      _vm._v(" "),
      _c(
        "button",
        { staticClass: "btn btn-primary btn-lg", attrs: { type: "button" } },
        [_vm._v("Large Button")]
      ),
      _vm._v(" "),
      _c(
        "button",
        { staticClass: "btn btn-secondary btn-lg", attrs: { type: "button" } },
        [_vm._v("Large Button")]
      ),
      _vm._v(" "),
      _c("br"),
      _vm._v(" "),
      _c(
        "button",
        { staticClass: "btn btn-primary", attrs: { type: "button" } },
        [_vm._v("Default Button")]
      ),
      _vm._v(" "),
      _c(
        "button",
        { staticClass: "btn btn-secondary", attrs: { type: "button" } },
        [_vm._v("Default Button")]
      ),
      _vm._v(" "),
      _c("br"),
      _vm._v(" "),
      _c(
        "button",
        { staticClass: "btn btn-primary btn-sm", attrs: { type: "button" } },
        [_vm._v("Small Button")]
      ),
      _vm._v(" "),
      _c(
        "button",
        { staticClass: "btn btn-secondary btn-sm", attrs: { type: "button" } },
        [_vm._v("Small Button")]
      )
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("p", [
      _vm._v("You can use "),
      _c("code", [_vm._v(".btn")]),
      _vm._v(" class on "),
      _c("code", [_vm._v("<a>")]),
      _vm._v(", "),
      _c("code", [_vm._v("<button>")]),
      _vm._v(" or "),
      _c("code", [_vm._v("<input>")]),
      _vm._v(" elements.")
    ])
  }
]
render._withStripped = true
var esExports = { render: render, staticRenderFns: staticRenderFns }
/* harmony default export */ __webpack_exports__["a"] = (esExports);
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-03ee345e", esExports)
  }
}

/***/ })

});