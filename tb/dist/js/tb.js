$(document).ready(function() {

	if ($('.tb-background').length) {
		$('.tb-background').parallax({
			'limit-y': 30,
			'limit-x': 30
		});
	}
	$(".fancybox").fancybox({
		helpers: {
			overlay: {
				locked: false
			}
		}
	});

	$('.tb-tabs').each(function() {
		var active = $(this).find('.tb-tabs__btn_active'),
			btn = active.length ? active : $(this).find('.tb-tabs__btn').first();

		tab($(this), btn);
	});

	$('body').on('click', '.tb-tabs__btn', function() {
		var tabs = $(this).closest('.tb-tabs');
		tab(tabs, $(this));
	});

	function tab(tabs, btn) {
		tabs.find('.tb-tabs__btn').removeClass('tb-tabs__btn_active');
		btn.addClass('tb-tabs__btn_active');
		tabs.find('.tb-tabs__tab').css('display', 'none');
		tabs.find('#'+btn.data('tab')).css('display', 'block');
	}
});