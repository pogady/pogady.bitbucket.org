$(document).ready(function() {
	$('body').on('click', '.mb-menu__btn', function() {
		$('.mb-menu').toggleClass('mb-menu_open');
	});
	$('body').on('click', '.mb-frame', function(e) {
		if (e.target.className != 'mb-menu__btn') {
			$('.mb-menu').removeClass('mb-menu_open');
		}
	});
});