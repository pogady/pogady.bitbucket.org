require('../img/favicon.ico');
//import videojs from 'video.js';
import WebFont from 'webfontloader';
import throttle from 'lodash/throttle';
import jQuery from 'jquery';
import Bowser from 'bowser';

window.$ = jQuery;

require('jquery.scrollbar');
require('viewport-units-buggyfill').init();

window.SM = function() {
  var constants = {
        video: 'sm-video',
        app_buttons: '.sm-buttons__badge',
        load_timeout: 1000
      },
      player = null,
      mobileHandled = false;

  function bindEvents() {
    // Setting common page-loaded class
    window.addEventListener('load', function() {
    $('html').addClass('page-loaded');
    })
    setTimeout(function() {
      $('html').addClass('page-loaded');
    }, constants.load_timeout)

    // Setting fonts-loaded classes
    WebFont.load({
      custom: {
        families: ['ANegaar', 'TanseekModPro']
      },
      timeout: constants.load_timeout
    });

    // Disable Some Links
    $('a[disabled]').on('click', function(e) {
      e.preventDefault();
      return false;
    })

    // GA Goals / Track App Buttons Clicking
    $(constants.app_buttons).on('click', function() {
      var goal = $(this).data('ga-goal'),
          disabled = $(this).attr('disabled');
      if (goal && typeof ga != undefined && !disabled) {
        ga('send', 'event', 'button', 'click', goal)
      }
    });

    // Handling window resizing
    $(window).on('resize', handleMobileDevice);

    // Init scrollbar with timeout in order to make sure
    // viewport-units-buggyfill've done his job properly
    setTimeout(function() {
      // Custom Scrollbar
      jQuery('.sm-scrollbar').scrollbar();
      // rtl bug fix
      $('body').children('div:not([class])').last().remove();
    }, 2000);
  }

  function initPlayer() {
    player = videojs(constants.video);
    player.ready(function() {
      this.on('click', function(e) {
        if (!this.isFullscreen() && !this.controlBar.el_.contains(e.target) && !this.paused()) {
          this.requestFullscreen();
        }
      })
    })
  }

  function detectClient() {
    function is(flag) {
      return flag !== undefined;
    }

    $('html')
      .toggleClass('ie', is(Bowser.msie) || is(Bowser.msedge))
      .toggleClass('m-safari', (is(Bowser.mobile) || is(Bowser.tablet)) && is(Bowser.safari))
      .toggleClass('firefox', is(Bowser.firefox))
      .toggleClass('macos', is(Bowser.mac))
      .toggleClass('ios', is(Bowser.ios))
      .toggleClass('win', is(Bowser.windows))


    $('html')
      .toggleClass('no-svg',
         is(Bowser.msie) ||
         is(Bowser.msedge) ||
        (is(Bowser.firefox) && Number(Bowser.version) == 56)
      )
  }

  // Disable overscroll on mobile devices
  function disableOverscroll() {
    // Uses document because document will be topmost level in bubbling
    $(document).on('touchmove',function(e) {
      e.preventDefault();
    });

    $('.sm-video__player').on('click', function() {
      $(this).removeClass('no-touch');
    })

    var scrolling = false;

    // Uses body because jquery on events are called off of the element they are
    // added to, so bubbling would not work if we used document instead.
    $('body').on('touchstart','.scroll-content',function(e) {

        // Only execute the below code once at a time
        if (!scrolling) {
            scrolling = true;
            if (e.currentTarget.scrollTop === 0) {
              e.currentTarget.scrollTop = 1;
            } else if (e.currentTarget.scrollHeight === e.currentTarget.scrollTop + e.currentTarget.offsetHeight) {
              e.currentTarget.scrollTop -= 1;
            }
            scrolling = false;
        }
    });

    // Prevents preventDefault from being called on document if it sees a scrollable div
    $('body').on('touchmove','.scroll-content',function(e) {
      e.stopPropagation();
    });
  }

  function handleMobileDevice() {
    if (window.innerWidth < 996 && !mobileHandled) {
      disableOverscroll();
      mobileHandled = true;
    }
  }

  (function init() {
    handleMobileDevice();
    bindEvents();
    detectClient();
    if (document.getElementById(constants.video) && videojs) {
      initPlayer();
    }
  })()

  return {
    player
  }
}()
