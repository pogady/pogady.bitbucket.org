var webpack = require('webpack'),
    path = require('path'),
    ExtractTextPlugin = require("extract-text-webpack-plugin"),
    isProduction = process.env.NODE_ENV === 'production';

module.exports = {
  entry: {
    sm: [
      './assets/src/js/sm.js',
      './assets/src/less/sm.less'
    ]
  },

  output: {
    path: path.resolve(__dirname, ''),
    publicPath: '/',
    filename: '[name].js'
  },

  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        loader: "babel-loader"
      },
      {
        test: /\.less$/,
        use: ExtractTextPlugin.extract({
          use: [
            {
              loader: 'css-loader',
              options: {
                minimize: isProduction
              }
            },
            'postcss-loader',
            'less-loader'
          ],
          fallback: 'style-loader'
        })
      },
      {
        test: /\.css$/,
        use: ['css-loader', 'postcss-loader']
      },
      {
        test: /\.(png|jpg|jpeg|gif|ico)$/,
        loader: 'file-loader',
        options: {
          name: '[name].[ext]',
          publicPath: ''
        }
      },
      {
        test: /\.(otf|eot|svg|ttf|woff|woff2)$/,
        loader: 'file-loader',
        options: {
          name: '[name].[ext]',
          publicPath: ''
        }
      }
    ]
  },

  plugins: [
    new ExtractTextPlugin('[name].css'),
    /*
    new webpack.optimize.CommonsChunkPlugin({
      name: "vendor",
      minChunks: function (module) {
        return module.context && module.context.indexOf("node_modules") !== -1;
      }
    })
    */
  ],

  devServer: {
    contentBase: path.join(__dirname, ""),
    compress: true,
    port: 9000,
    quiet: true
  }
};

if (isProduction) {
  module.exports.plugins = module.exports.plugins.concat([
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: '"production"'
      }
    }),
    new webpack.optimize.UglifyJsPlugin({
      sourceMap: true,
      compress: {
        warnings: false
      }
    })
  ])
}
