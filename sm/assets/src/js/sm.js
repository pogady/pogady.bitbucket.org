require('../img/favicon.ico');
require('classlist-polyfill');
import videojs from 'video.js';
import WebFont from 'webfontloader';

window.SM = function() {
  var constants = {
        video: 'sm-video',
        app_buttons: 'sm-apps__badge',
        load_timeout: 1000
      },
      player = null;

  function bindEvents() {
    // Setting common page-loaded class
    window.addEventListener('load', function() {
      document.documentElement.classList.add('page-loaded');
    })
    setTimeout(function() {
      document.documentElement.classList.add('page-loaded');
    }, constants.load_timeout)

    // Setting fonts-loaded classes
    WebFont.load({
      custom: {
        families: ['ANegaar', 'TanseekModPro']
      },
      timeout: constants.load_timeout
    });

    // Disable Some Links
    Array.from(document.querySelectorAll('a[disabled]')).forEach(function(link) {
      link.addEventListener('click', function(e) {
        e.preventDefault();
        return false;
      })
    })

    // GA Goals / Track App Buttons Clicking
    Array.from(document.getElementsByClassName(constants.app_buttons)).forEach(function(button) {
      button.addEventListener('click', function() {
        var goal = this.getAttribute('data-ga-goal'),
            disabled = this.getAttribute('disabled');
        if (goal && ga && disabled === null) {
          ga('send', 'event', 'button', 'click', goal);
        }
      });
    });
  }

  function initPlayer() {
    player = videojs(constants.video);
    player.ready(function() {
      this.on('click', function(e) {
        if (!this.isFullscreen() && !this.controlBar.el_.contains(e.target) && !this.paused()) {
          this.requestFullscreen();
        }
      })
    })
  }

  function detectIE() {
    var agent = window.navigator.userAgent;
    if ((agent.indexOf('MSIE ') != -1) || (agent.indexOf('Trident/') != -1) || (agent.indexOf('Edge/') != -1)) {
      document.documentElement.classList.add('ie');
    }
  }

  (function init() {
    bindEvents();
    detectIE();
    if (document.getElementById(constants.video)) {
      initPlayer();
    }
  })()

  return {
    player
  }
}()
