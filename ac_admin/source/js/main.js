$(document).ready(function() {
	$('#auth__phone').mask("7 ( 999 ) - 999 - 99 - 99");

	var Shops = function(url) {
		this.url = url;
		this.items = [];
		this.init();
	};

	Shops.prototype = {
		init: function() {
			var that = this;
			$.ajax({
				type: 'GET',
				url: this.url,
				dataType: 'json',
				success: function(data) {
					that.updateItems(data);
				}
			});
		},
		updateItems: function(items) {
			this.items = items;
		},
		getShopById: function(id) {
			var shop = {};
			this.items.forEach(function(item) {
				console.log(item);
				if (item.id == id) {
					shop = item;
				}
			});
			return shop;
		}
	}

	var ac_shops = new Shops('./js/shops.json');


	$('.selectpicker').selectpicker();
	$('.ac-reviews__rating input').rating({
		stars: 5,
		min: 0,
		max: 5,
		step: 1,
		showClear: false,
		showCaption: false
	});

	$('body').on('click', '.ac-prices__new button', function() {
		var prices = $(this).closest('.ac-prices').find('.ac-prices__list'),
			editable = $(this).data('edit') ? true : false;
		if (prices.length) {
			var price =  $(document.createElement('div')).addClass('ac-prices__price');
			price.html(''+
				'<div class="ac-prices__shop">'+
				'</div>'+
				'<div class="ac-prices__value">'+
					'<div class="ac-prices__control">'+
						'<input class="form-control'+(editable ? ' ac-editable__input' : '')+'" type="text" value="0"/>'+
						(editable ? '<div class="ac-product__text ac-editable__text"></div>' : '')+
					'</div>'+
					'<div class="ac-prices__label">'+
						'руб.'+
					'</div>'+
				'</div>'+
				'<div class="ac-prices__value">'+
					'<div class="ac-prices__control">'+
						'<input class="form-control'+(editable ? ' ac-editable__input' : '')+'" type="text" value="0"/>'+
						(editable ? '<div class="ac-product__text ac-editable__text"></div>' : '')+
					'</div>'+
					'<div class="ac-prices__label">'+
						'коп.'+
					'</div>'+
				'</div>'+
				'');
			if (editable) {
				price.addClass('ac-product__value ac-editable ac-editable_edit');
				price.append('' +
					'<div class="ac-prices__action">' +
						'<button class="ac-editable__save" type="button">Сохранить</button>' +
					'</div>' +
					'<div class="ac-prices__action">' +
						'<button class="ac-editable__edit" type="button">Редактировать</button>' +
					'</div>' +
					'<div class="ac-prices__action">' +
						'<button class="ac-editable__delete" type="button">×</button>' +
					'</div>' +
					'');
			}
			prices.append(price);
			var select = $(document.createElement('select')).addClass('selectpicker');
			ac_shops.items.forEach(function(shop) {
				select.append('<option value="'+shop.id+'">'+shop.name+'</option>');
			});
			price.find('.ac-prices__shop').append(select);
			select.selectpicker();
		}
	});

	$('body').on('click', '.ac-editable__delete', function(){
		var field = $(this).closest('.ac-editable');
		if (field.length) {
			field.remove();
		}
		return false;
	})

	$('body').on('click', '.ac-editable__edit', function() {
		var field = $(this).closest('.ac-editable'),
			input = field.find('.ac-editable__input');
		if (field.length) {
			field.addClass('ac-editable_edit');
			input.focus();
			if (input[0].setSelectionRange) {
				input[0].setSelectionRange(input.val().length, input.val().length);
			} else {
				input.focus().val(input.val());
			}
		}
		return false;
	});

	$('body').on('click', '.ac-editable__save', function() {
		var field = $(this).closest('.ac-editable'),
			select = field.find('.ac-prices__shop select.selectpicker');
		if (field.length) {
			field.removeClass('ac-editable_edit');
			var input = field.find('.ac-editable__input'),
				text = field.find('.ac-editable__text');
			if (input.length && text.length) {
				input.each(function() {
					$(this).siblings('.ac-editable__text').text($(this).val());
				})
			}
			if (select.length) {
				var value = select.selectpicker('val'),
					shop = ac_shops.getShopById(value);
				select.parent().html('<label>'+shop.name+'</label><input class="ac-prices__shop-input" value="'+shop.id+'"/>');
			}
		}
		return false;
	});

	$('body').on('click', '.ac-product__photo-delete', function() {
		var cover = $(this).closest('.ac-product__cover'),
			photo = $(this).closest('.ac-product__photo');
		photo.removeAttr('style');
		cover.addClass('ac-product__cover_empty');
		return false;
	})
});