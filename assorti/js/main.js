$(document).ready(function() {
	$('.as-slider__slides').slick({
		dots: true,
		infinite: true,
		slidesToShow: 1,
		prevArrow: '.as-slider__next',
		nextArrow: '.as-slider__prev',
		customPaging: function(slider, i) {
			return '<div class="as-slider__dot">' + (i + 1) + '</div>';
		},
		autoplay: true,
		autoplaySpeed: 5000
	});
	$('.as-cats__cont').slick({
		infinite: true,
		slidesToShow: 3,
		slidesToScroll: 3,
		dots: false,
		prevArrow: '.as-cats__left',
		nextArrow: '.as-cats__right',
		speed: 600
	});
});