/**
 * Overview page module.
 * @module pages/overview
 * @author AdminBootstrap.com
 */

App.page.overview = function() {

  /**
   * @const {object} Constant css-selectors to link up with HTML markup.
   */
  var c = {}

  /**
   * @var {object} Module settings object.
   */
  var s = {
    widgets: {}
  };

  function initWidgets() {
    // DetailedChart Widget
    s.widgets.dc = new App.classes.DetailedChart();
    // Activity Feed Widget
    s.widgets.afeed = new App.classes.ActivityFeed();
  }

  function initPlugins() {
    // widsbar horizontal scroll
    //$('.st-widsbar__cont').scrollbar({ disableBodyScroll: true, duration: 50 });
    // Init jQuery.Sparkline Charts
    $('.sparkline').sparkline('html', { enableTagOptions: true });
    $(document.getElementById('resize-iframe').contentWindow).on('resize', function() {
      $('.sparkline').sparkline('html', { enableTagOptions: true });
    })
    // Init Chart.js Doughnuts
    $('.chartjs-doughnut').each(function() {
      new Chart(this, {
          type: 'doughnut',
          data: {
            datasets: [
              {
                data: $(this).data('set'),
                backgroundColor: [
                    "#59CD90",
                    "#45BDDC",
                    "#F7B267"
                ],
                hoverBackgroundColor: [
                    "#59CD90",
                    "#45BDDC",
                    "#F7B267"
                ],
                hoverBorderColor: ['', '', '']
              }]
          },
          options: {
            layout: {
              padding: -3
            },
            legend: {
              display: false
            },
            tooltips: {
              enabled: false
            },
            animation: {
              duration: 0
            }
          }
      });
    })
  }

  function bindUIActions() {
    $(window).on('resize', function() {
      $('.sparkline').sparkline('html', { enableTagOptions: true });
    })
  }

  function init() {
    initPlugins();
    initWidgets();
    bindUIActions();
  }

  init();

  return s;

}();
