$(document).on('submit', '.contact__form', function(event) {  // перехватываем все при событии отправки
	var form = $(this); // запишем форму, чтобы потом не было проблем с this
	var error = false; // предварительно ошибок нет
	var url = form.attr('action');
	form.find('input, textarea, select').each( function(){ // пробежим по каждому полю в форме
		$(this).closest('.contact__field').removeClass('error');
		if ($(this).val() == '' && $(this).hasClass('required')) { // если находим пустое
			$(this).closest('.contact__field').addClass('error');
			error = true; // ошибка
		} else {
			$(this).removeClass('error');
		}
	});
	if (!error) { // если ошибки нет
		var data = form.serialize(); // подготавливаем данные
		form.addClass('form-send');

		/*
		$.ajax({ // инициализируем ajax запрос
			type: 'POST', // отправляем в POST формате, можно GET
			url: url, // путь до обработчика, у нас он лежит в той же папке
			dataType: 'json', // ответ ждем в json формате
			data: data, // данные для отправки
			beforeSend: function(data) { // событие до отправки
				form.find('input[type="submit"]').attr('disabled', 'disabled'); // например, отключим кнопку, чтобы не жали по 100 раз
			},
			success: function(data){ // событие после удачного обращения к серверу и получения ответа
				if (data['error']) { // если обработчик вернул ошибку
					alert(data['error']); // покажем её текст
				} else { // если все прошло ок
					form.closest('.form').addClass('form-send');

					// Скрываем форму
					setTimeout(function() {
						form.closest('.b-write-letter-form').animate(
							{height: 0}, 600, function() {
								setTimeout(function() {
									form.closest('.b-write-letter').removeClass('-open');
								}, 200);

							});
					}, 3400);

					// Очищаем поля
					setTimeout(function() {
						form.find('input[type="text"], textarea').val('');
						form.closest('.form').removeClass('form-send');
					}, 4000);
				}
			},
			error: function (xhr, ajaxOptions, thrownError) { // в случае неудачного завершения запроса к серверу
				alert(xhr.status); // покажем ответ сервера
				alert(thrownError); // и текст ошибки
			},
			complete: function(data) { // событие после любого исхода
				form.find('input[type="submit"]').prop('disabled', false); // в любом случае включим кнопку обратно
			}

		});
		*/
	}
	return false; // вырубаем стандартную отправку формы
});
