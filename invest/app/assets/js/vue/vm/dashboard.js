var ovWidget = new Vue({
	el: '#ov-widget',
	data: {
		items: null
	},
	created: function () {
		this.fetchData();
	},
	methods: {
		fetchData: function () {
			var xhr = new XMLHttpRequest()
			var self = this
			xhr.open('GET', 'data/statistic/overview.json')
			xhr.onload = function () {
				self.items = JSON.parse(xhr.responseText)
			}
			xhr.send()
		}
	}
});

// Levels chart
var levelsChart = new Vue({
	el: '#levelsChart',
	data: {
		levels: null
	},
	created: function() {
		this.fetchData();
	},
	methods: {
		fetchData: function() {
			var xhr = new XMLHttpRequest()
			var self = this
			xhr.open('GET', 'data/statistic/users.json')
			xhr.onload = function () {
				self.levels = JSON.parse(xhr.responseText).levels
			}
			xhr.send()
		}
	}
});

// Ratio chart
var ratioChart = new Vue({
	el: '#ratioChart',
	data: {
		ratio: null
	},
	created: function() {
		this.fetchData();
	},
	methods: {
		fetchData: function() {
			var xhr = new XMLHttpRequest()
			var self = this
			xhr.open('GET', 'data/statistic/users.json')
			xhr.onload = function () {
				self.ratio = JSON.parse(xhr.responseText).ratio
			}
			xhr.send()
		}
	}
});

// Feed Widget
var feedWidget = new Vue({
	el: '#feedWidget',
	template: '#feedWidgetTpl',
	data: {
		feed: null
	},
	created: function() {
		this.fetchData();
	},
	methods: {
		fetchData: function() {
			var xhr = new XMLHttpRequest()
			var self = this
			xhr.open('GET', 'data/feed/feed.json')
			xhr.onload = function () {
				self.feed = JSON.parse(xhr.responseText)
			}
			xhr.send()
		}
	},
	filters: {
		formatDate: function(value) {
			return new Date(value).toDateString();
		}
	}
});

// Support Widget
var supportWidget = new Vue({
	el: '#supportWidget',
	template: '#supportWidgetTpl',
	data: {
		support: []
	},
	created: function() {
		this.fetchData();
	},
	methods: {
		fetchData: function() {
			var xhr = new XMLHttpRequest()
			var self = this
			xhr.open('GET', 'data/messages/messages.json')
			xhr.onload = function () {
				var messages = JSON.parse(xhr.responseText);
				messages.forEach(function(msg) {
					if (msg.tag == 'support') {
						self.support.push(msg);
					}
				})
			}
			xhr.send()
		}
	},
	filters: {
		formatDate: function(value) {
			return new Date(value).toDateString();
		},
		maxLength: function(value, length) {
			if (value.length > length) {
				value = value.slice(0, length)+'..'
			}
			return value;
		}
	}
});
