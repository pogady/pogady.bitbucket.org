// User
var user = new Vue({
	el: '#user',
	data: {
		user: {
			admin: false,
			name: 'mr.Anderson'
		}
	},
	created: function() {
		this.fetchData();
	},
	methods: {
		fetchData: function() {
			var xhr = new XMLHttpRequest()
			var self = this
			xhr.open('GET', 'data/statistic/sidestat.json')
			xhr.onload = function () {
				self.items = JSON.parse(xhr.responseText)
			}
			xhr.send()
		}
	}
});

user.$watch('user.admin', function (val) {
	this.user.name = val ? 'Administrator' : 'mr.Anderson';
})