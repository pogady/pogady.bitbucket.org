// Recent Activity Widget
var raWidget = new Vue({
	el: '#raWidget',
	template: '#raWidgetTpl',
	data: {
		feed: []
	},
	created: function() {
		this.fetchData();
	},
	methods: {
		fetchData: function() {
			var xhr = new XMLHttpRequest()
			var self = this
			xhr.open('GET', 'data/feed/feed.json')
			xhr.onload = function () {
				self.feed = JSON.parse(xhr.responseText)
			}
			xhr.send()
		}
	},
	filters: {
		formatDate: function(value) {
			return new Date(value).toDateString();
		}
	}
});


// Last Messages Widget
var lmWidget = new Vue({
	el: '#lmWidget',
	template: '#lmWidgetTpl',
	data: {
		messages: {
			new: [],
			last: []
		}
	},
	created: function() {
		this.fetchData();
	},
	methods: {
		fetchData: function() {
			var xhr = new XMLHttpRequest()
			var self = this
			xhr.open('GET', 'data/messages/messages.json')
			xhr.onload = function () {
				var messages = JSON.parse(xhr.responseText)
				messages.forEach(function(msg) {
					if (msg.new) {
						self.messages.new.push(msg);
					} else {
						self.messages.last.push(msg);
					}
				})
			}
			xhr.send()
		}
	}
});

// Side Statistic
var sideStat = new Vue({
	el: '#sideStat',
	data: {
		items: [],
		types: {
			'bar': {
				type: 'bar',
				height: '30px',
				barSpacing: 2,
				barColor: '#1e59d9',
				negBarColor: '#ed4949'
			},
			'area': {
				width: '145px',
				height: '40px',
				type: 'line',
				lineColor: '#3273B1',
				lineWidth: 2,
				fillColor: 'rgba(50,115,177, 0.6)',
				spotColor: '#3273B1',
				minSpotColor: '#3273B1',
				maxSpotColor: '#3273B1',
				highlightSpotColor: '#3273B1',
				spotRadius: 2
			},
			'bar_thin': {
				type: 'bar',
				height: '30px',
				barSpacing: 1,
				barWidth: 2,
				barColor: '#FED42A',
				negBarColor: '#ed4949'
			},
			'line': {
				type: 'bar',
				height: '30px',
				barSpacing: 2,
				barWidth: 3,
				barColor: '#20c05c',
				negBarColor: '#ed4949'
			}
		}
	},
	created: function() {
		this.fetchData();
	},
	methods: {
		fetchData: function() {
			var xhr = new XMLHttpRequest()
			var self = this
			xhr.open('GET', 'data/statistic/sidestat.json')
			xhr.onload = function () {
				self.items = JSON.parse(xhr.responseText)
			}
			xhr.send()
		}
	}
});