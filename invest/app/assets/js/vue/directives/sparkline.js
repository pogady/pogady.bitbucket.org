Vue.directive('sparkline', function (value) {
	if (!value.options || !value.data) {
		return false;
	}

	var el = $(this.el);
	setTimeout(function() {
		el.sparkline(value.data,value.options);
	}, 0);
});