Vue.directive('morris', function (value) {
	if (!value.options) {
		return false;
	}

	if (!this.morris) {
		switch (value.type) {
			case 'line':
				var options = value.options;
				var dataKeys = [];
				for ( var key in options.data[0]) {
					if (!options.data[0].hasOwnProperty(key)) continue;
					dataKeys.push(key);
				}
				var month = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'June', 'July', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec'];

				this.morris = Morris.Line({
					element: this.el,
					data: options.data,
					xkey: dataKeys.shift(),
					ykeys: dataKeys,
					dateFormat: function (x) {
						return new Date(x).toDateString();
					},
					xLabelFormat: function (x) {
						return month[new Date(x).getMonth()];
					},
					labels: dataKeys,
					lineColors: options.colors,
					pointSize: 0,
					pointStrokeColors: options.colors,
					lineWidth: 3,
					resize: true
				});
				break;
			case 'donut':
				var options = value.options;

				this.morris = Morris.Donut({
					element: this.el,
					data: options.data,
					colors: options.colors,
					labelColor: options.labelColor,
					resize: true
				});
				break;
		}
	} else {
		this.morris.setData(value.options.data);
	}
});