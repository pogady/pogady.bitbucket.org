$(document).ready(function() {
	// Table tab count update
	function tabInfo(table) {
		var id = $(table).closest('.tab-pane').attr('id'),
			tab = $('.nav-tabs a[aria-controls='+id+']'),
			length = $(table).DataTable().page.info().recordsDisplay,
			label = tab.find('span.label');
		if (label.length) { label.remove(); }
		tab.append('<span class="label">'+length+'</span>');
	}

	// Preview update
	function previewUpdate(data) {
		var user = $('.users-preview');
		user.find('.users-preview__name').text(data[1]).attr('title', data[1]);
		user.find('.users-preview__location').text(data[2]).attr('title', data[2]);
		user.find('.users-preview__contact').text(data[3]).attr('title', data[3]);
		user.find('.users-preview__date').text(data[4]).attr('title', data[4]);
		user.find('.users-preview__position').text(data[6]).attr('title', data[6]);
		user.find('.users-preview__status').text(data[8]).attr('title', data[8]);

		user.find('.users-preview__stat').sparkline(
			JSON.parse('['+data[7]+']'),
			{
				type: 'bar',
				height: '34px',
				barSpacing: 2,
				barColor: '#1e59d9',
				negBarColor: '#ed4949'
			}
		);
	}

	var tables = $('.datatable')
		.on('preInit.dt', function (e, settings) {
			var api = new $.fn.dataTable.Api( settings),
				users = $(api.table().node()).data('users');
			api.ajax.url('data/users/'+users+'.json');
		})
		.on('init.dt', function () {
			tabInfo(this);
			var previewData = $.fn.dataTable.tables( {visible: true, api: true}).rows(0).data()[0];
			previewUpdate(previewData);
		})
		.on('draw.dt', function () {
			tabInfo(this);
		})
		.on('search.dt', function () {
			tabInfo(this);
		})
		.DataTable({
			ordering: true,
			lengthChange: false,
			pagingType: 'numbers',
			select: {
				style: 'single'
			},
			columnDefs: [
				{
					"targets": [ 6 ],
					"visible": false
				},
				{
					"targets": [ 7 ],
					"visible": false
				}
			]
		})
		.on( 'select', function ( e, dt, type, indexes ) {
			var data = $(this).DataTable().rows( indexes ).data()[0];
			previewUpdate(data);
		});


	$('a[data-toggle="tab"]').on( 'shown.bs.tab', function (e) {
		$.fn.dataTable.tables( {visible: true, api: true} ).columns.adjust();
	} );

	$('.datalist-filter__search input').on( 'keyup', function () {
		tables.search( this.value ).draw();
	} );

});