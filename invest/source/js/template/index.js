$(document).ready(function() {

	if ($('.pages_dashboard').length) {
		var month = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'June', 'July', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec'];
		if ($('.ld-widget-main__chart').length) {
			var levels = Morris.Line({
				element: $('.ld-widget-main__chart'),
				data: [],
				xkey: 'd',
				ykeys: ['a', 'b', 'c'],
				dateFormat: function (x) {
					return new Date(x).toDateString();
				},
				xLabelFormat: function (x) {
					return month[new Date(x).getMonth()];
				},
				labels: ['Light', 'Pro', 'Free'],
				lineColors: ['#ed4949', '#FED42A', '#20c05c', '#1e59d9'],
				pointSize: 0,
				pointStrokeColors: ['#ed4949', '#FED42A', '#20c05c', '#1e59d9'],
				lineWidth: 3,
				resize: true
			});
		}
		if ($('.ld-widget-side__chart').length) {
			var ratio = Morris.Donut({
				element: $('.ld-widget-side__chart'),
				data: [
					[{"value":"","label":""}]
				],
				colors: ['#ed4949', '#FED42A', '#20c05c', '#1e59d9'],
				backgroundColor: '#30363c',
				labelColor: '#88939C',
				resize: true
			});
		}
		if (levels != undefined && ratio != undefined) {
			$.getJSON('data/statistic/users.json', function(data){
				if (data.levels.length) {
					levels.setData(data.levels);
				}
				if (data.ratio.length) {
					ratio.setData(data.ratio);
					$('.ld-widget-side__footer').html('');
					data.ratio.forEach(function(item) {
						$('.ld-widget-side__footer').append('<div class="ld-widget-side__item"><div class="ld-widget-side__label">'+item.label+'</div><div class="ld-widget-side__value">'+item.value+'</div></div>');
					});
				}
			});
		}

		/*
		if ($('.ov-widget__list').length) {
			$.getJSON('data/statistic/overview.json', function(data){
				if (data.data.length) {
					$('.ov-widget__list').html('');
					data.data.forEach(function(item) {
						if (item.type == 'bar') {
							$('.ov-widget__list').append('<div class="ov-widget__bar"><span> '+item.label+'</span><div class="progress"><div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="'+item.value+'" aria-valuemin="0" aria-valuemax="100" style="width: '+item.value+'%"></div></div></div>');
						} else {
							$('.ov-widget__list').append('<div class="ov-widget__item ov-widget__item_'+item.type+'"><div class="ov-widget__value"> '+item.value+'</div><div class="ov-widget__info"><div class="ov-widget__title"> '+item.label+'</div><div class="ov-widget__change"><span> '+item.change+'</span><span class="fa fa-level-up"></span><span class="fa fa-level-down"></span><span class="fa fa-bolt"></span><span class="fa fa-thumb-tack"></span></div></div></div>');
						}
					});
				}
			});
		}
		*/



		$('.selectpicker').selectpicker();


	}
});