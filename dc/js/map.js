ymaps.ready(init);
var myMap;

function init(){
	myMap = new ymaps.Map("map", {
		center: [55.76, 37.64],
		zoom: 14
	}),
	objectManager_evacuators = new ymaps.ObjectManager({}),
	objectManager_incidents = new ymaps.ObjectManager({});

	objectManager_evacuators.objects.options.set({"iconLayout": "default#image", "iconImageHref": "img/mark_evacuator.png", "iconImageSize": [73, 81], "iconImageOffset": [-30, -70]});
	objectManager_incidents.objects.options.set({"iconLayout": "default#image", "iconImageHref": "img/mark_incident.png", "iconImageSize": [73, 81], "iconImageOffset": [-30, -70]});

	myMap.geoObjects.add(objectManager_evacuators);
	myMap.geoObjects.add(objectManager_incidents);

	$.ajax({
		url: "js/mark_evacuators.json"
	}).done(function(data) {
		objectManager_evacuators.add(data);
	});

	$.ajax({
		url: "js/mark_incidents.json"
	}).done(function(data) {
		objectManager_incidents.add(data);
	});
}

