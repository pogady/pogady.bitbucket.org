$(document).ready(function() {
	$('body').on('click', '.bottom__up-link', function() {
		$('html, body').stop().animate({scrollTop: 0}, 400);
	});

	jQuery('.scrollbar-inner').scrollbar();

	$('body').on('click', '.feed__item', function() {
		$('.feed__look').fadeIn(30);
	})

	$('body').on('click', '.feed__look', function() {
		$(this).fadeOut(30);
	})

	$('.how__slider').slick({
		dots: true,
		infinite: true,
		slidesToShow: 1,
	});
});