$(document).ready(function() {
	$(window).on('scroll', function() {
		var up = $('.ort-up'),
			LIMIT = 300;
		if ($(window).scrollTop() >= LIMIT && up.hasClass('ort-up_hidden')) {
			up.fadeIn(200, function() {
				$(this).removeClass('ort-up_hidden');
			});
		} else if ($(window).scrollTop() < LIMIT && !up.hasClass('ort-up_hidden')) {
			up.fadeOut(200, function() {
				$(this).addClass('ort-up_hidden');
			});
		}
	}).scroll();

	$('body').on('click', '.ort-up button', function() {
		$('html, body').stop().animate({scrollTop: 0}, 200);
	});

	$('body').on('click', '.ort-menu__btn', function() {
		$('.ort-menu').slideToggle(300);
	});

	$(window).on('resize', function() {
		$('.ort-menu').attr('style', '');
	});
});