videojs('ort-profile-video', {
	controls: true,
	width: '100%',
	poster: '../media/poster.png',
	controlBar: {
		fullscreenToggle: false,
		progressControl: false
	},
	plugins: {
		videoJsResolutionSwitcher: {
			default: '720',
			dynamicLabel: true
		},
		videoJsDownloader: {label: 'Download'},
		videoJsFavorite: {favorite: true},
		videoJsWatermarks: {watermarks: ['star', 'something']}
	}
}, function(){
	var player = this;
	window.player = player;
	player.updateSrc([
		{
			src: '../media/video_720.mp4',
			type: 'video/mp4',
			label: '720',
			res: 720
		},
		{
			src: '../media/video_360.mp4',
			type: 'video/mp4',
			label: '480',
			res: 360
		},
		{
			src: '../media/video_360.mp4',
			type: 'video/mp4',
			label: '280',
			res: 360
		}
	]);

	player.updateDownload([
		{
			src: '../media/video_720.mp4',
			type: 'video/mp4',
			q: 'lq',
			size: '750 Mb'
		},
		{
			src: '../media/video_360.mp4',
			type: 'video/mp4',
			q: 'mq',
			size: '2309 Mb'
		},
		{
			src: '../media/video_360.mp4',
			type: 'video/mp4',
			q: 'hq',
			size: '4509 Mb'
		}
	])
});
