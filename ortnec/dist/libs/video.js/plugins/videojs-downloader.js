(function(window, videojs) {
	'use strict';

	var defaults = {},
		videoJsDownloader;

	/**
	 * Initialize the plugin.
	 * @param options (optional) {object} configuration for the plugin
	 */
	videoJsDownloader = function(options) {
		var settings = videojs.mergeOptions(defaults, options),
			player = this,
			downloads = [],
			btnLabel = options.label,
			link = document.createElement('a');

		link.classList.add('vjs-download-link');
		link.setAttribute('download', '');
		player.el().appendChild(link);

		/*
		 * Download menu item
		 */
		var MenuItem = videojs.getComponent('MenuItem');
		var DownloadMenuItem = videojs.extend(MenuItem, {
			constructor: function(player, options){

				MenuItem.call(this, player, options);
				this.src = options.src;
				this.type = options.type;
				this.q = options.q;
				this.size = options.size;
				this.el_.innerHTML = '<span class="vjs-download-item-label">'+this.q+'</span><span class="vjs-download-item-size">'+this.size+'</span>';

				this.on('click', this.onClick);
				this.on('touchstart', this.onClick);
			},
			onClick: function(){
				link.href = this.src;
				link.click();
			}
		});


		/*
		 * Download menu button
		 */
		var MenuButton = videojs.getComponent('MenuButton');
		var DownloadMenuButton = videojs.extend(MenuButton, {
			constructor: function(player, options){
				this.sources = options.sources;
				MenuButton.call(this, player, options);
				this.controlText('Download');
				var label = document.createElement('span');
				label.classList.add('vjs-download-label');
				label.innerHTML = btnLabel ? btnLabel : 'Download';
				this.el().appendChild(label);
			},
			createItems: function(){
				var sources = this.sources;
				var menuItems = [];
				for(var i = 0; i < sources.length; i++){
					menuItems.push( new DownloadMenuItem(player, {
						q: sources[i].q,
						src: sources[i].src,
						type: sources[i].type,
						size: sources[i].size
					}));
				}
				return menuItems;
			}
		});


		player.updateDownload = function(src){
			// Dispose old download menu button before adding new downloads
			if(player.controlBar.downloader){
				player.controlBar.downloader.dispose();
				delete player.controlBar.downloader;
			}
			//Create Download button
			var menuButton = new DownloadMenuButton(player, { sources: src });
			menuButton.el().classList.add('vjs-download-button');
			player.controlBar.downloader = player.controlBar.addChild(menuButton);
			downloads = src;
		};

		// Create downloader for videos form <source> tag inside <video>
		player.ready(function() {
			if(downloads.length == 0 && player.options_.sources.length > 0){
				player.updateDownload(player.options_.sources);
				console.log(player.options_.sources);
			}
		});

	};

	// register the plugin
	videojs.plugin('videoJsDownloader', videoJsDownloader);
})(window, window.videojs);