// Favorite Plugin

(function(window, videojs) {
	'use strict';

	var videoJsFavorite = function(options) {
		var settings = {
				favorite: options && options.favorite ? true : false
			},
			player = this;

		var Button = videojs.getComponent('Button');
		var FavoriteButton = videojs.extend(Button, {
			constructor: function(player, options){

				Button.call(this, player, options);
				this.addClass('vjs-favorite-button');
				this.selected = settings.favorite;
				if (this.selected) this.addClass('vjs-selected');
				this.on('click', this.onClick);
			},

			onClick: function(){
				this.toggleButton()
			},

			toggleButton: function() {
				if (this.selected) {
					this.selected = false;
					this.removeClass('vjs-selected');
				} else {
					this.selected = true;
					this.addClass('vjs-selected');
				}
			}
		});

		this.toggleFavorite = function() {
			player.favoriteButton.toggleButton();
		}

		this.setState = function(state) {
			if (!state) {
				player.favoriteButton.selected = false;
				player.favoriteButton.removeClass('vjs-selected');
			} else {
				player.favoriteButton.selected = true;
				player.favoriteButton.addClass('vjs-selected');
			}
		}

		player.ready(function() {
			if(!player.favoriteButton){
				player.favoriteButton = new FavoriteButton(player, {});
				player.controlBar.downloader = player.controlBar.addChild(player.favoriteButton);
			}
		});

		return this;

	};

	videojs.plugin('videoJsFavorite', videoJsFavorite);
})(window, window.videojs);

// Watermarks Plugin

(function(window, videojs) {
	'use strict';

	var videoJsWatermarks = function(options) {
		var settings = {
				watermarks: options && options.watermarks ? options.watermarks : []
			},
			player = this;

		for(var i=0; i<settings.watermarks.length; i++) {
			var mark = document.createElement('div');
			mark.classList.add('vjs-watermark');
			mark.classList.add('vjs-watermark-'+settings.watermarks[i]);
			player.el().appendChild(mark);
		}

		return this;

	};

	videojs.plugin('videoJsWatermarks', videoJsWatermarks);
})(window, window.videojs);