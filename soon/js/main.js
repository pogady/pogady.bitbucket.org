$(document).ready(function() {


	if ($('.soon-timer').length > 0) {
		$('.soon-timer').countdown({
			end_time: "2016/02/12 23:05:00 +0600",
			wrapper: function (unit) {
				var wrpr = $('<div></div>').
					addClass('soon-timer__section').
					addClass('soon-timer__section_' + unit.toLowerCase());
				$('<div class="counter soon-timer__digits"></div>').appendTo(wrpr);
				$('<div class="soon-timer__title">' + unit.toLowerCase() + 's</div>').appendTo(wrpr);
				return wrpr;
			}
		});
	}
});